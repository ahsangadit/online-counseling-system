@extends('layouts.customer_layout.customer_design')


@section('title')
    Subscribed Counselors
@endsection

@section('css-content')
    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css")}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset("vendors/backend_vendors/jqvmap/dist/jqvmap.min.css")}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">

    <!-- Datatables -->
    <link href="{{ asset("vendors/backend_vendors/datatables.net-bs/css/dataTables.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css")}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">
@endsection


@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Counselors</h3>
                </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Details</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Counselor:</th>
                                    <th>Email:</th>
                                    <th>Status:</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($subscribers)
                                    @foreach($subscribers as $subscriber)
                                        <tr>
                                            <td>{{$subscriber->name}}</td>
                                            <td>{{$subscriber->email}}</td>
                                            <td>{{$subscriber->status}}</td>

                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection

@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- Datatables -->
    <script src="{{ asset("vendors/backend_vendors/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/dataTables.buttons.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.flash.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.html5.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.print.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-keytable/js/dataTables.keyTable.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-responsive/js/dataTables.responsive.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-scroller/js/dataTables.scroller.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jszip/dist/jszip.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pdfmake/build/pdfmake.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pdfmake/build/vfs_fonts.js")}}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>
@endsection

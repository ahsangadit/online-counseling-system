@extends('layouts.customer_layout.customer_design')


@section('title')
    Join Events
@endsection


@section('css-content')
    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.css")}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset("vendors/backend_vendors/select2/dist/css/select2.min.css")}}" rel="stylesheet">
    <!-- PNotify -->
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.css")}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

@endsection



@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Extra</h3>
                </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Join Events & Seminars <small class="label label-danger"> Event Page is pending </small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection

@section('js-content')

    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- morris -->
    <script src="{{ asset("vendors/backend_vendors/raphael/raphael.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/morris/morris.js")}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
    {{--<!-- bootstrap-daterangepicker -->--}}
    {{--<script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>--}}
    {{----}}
    {{--<script src="{{ asset("vendors/backend_vendors/morris/morris.js")}}"></script>--}}
    <!-- dropzone -->
    <script src="{{ asset("vendors/backend_vendors/dropzone/dist/dropzone.js")}}"></script>
    <!-- pnotify -->
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>

    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>
@endsection


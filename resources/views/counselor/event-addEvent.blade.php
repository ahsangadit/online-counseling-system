@extends('layouts.counselor_layout.counselor_design')


@section('title')
    Add Event
@endsection


@section('css-content')

    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset("vendors/backend_vendors/google-code-prettify/bin/prettify.min.css")}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset("vendors/backend_vendors/select2/dist/css/select2.min.css")}}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.css")}}" rel="stylesheet">
    <!-- starrr -->
    <link href="{{ asset("vendors/backend_vendors/starrr/dist/starrr.css")}}" rel="stylesheet">

    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}"  rel="stylesheet">
    <!-- PNotify -->
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.css")}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

@endsection

@section('content')
    <?php
    //    use App\Counselor;
    //    $user_further_detail = Counselor::where(["User_id"=> $user_detail->id])->first();


    ?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Event</h3>
                </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Service</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @if (Session::has('notifier.notice'))
                            <script>
                                new PNotify({!! Session::get('notifier.notice') !!});
                            </script>
                        @endif

                        <div class="x_content">

                            <p class="font-gray-dark">
                                Using the grid system you can control the position of the labels. The form example below has the <b>col-md-10</b> which sets the width to 10/12 and <b>center-margin</b> which centers it.
                            </p>

                            <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" action="{{url('/Counselor/Update-Profile-Picture')}}">

                            </form>
                            <div class="ln_solid"></div>


                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data"  method="post" action="{{url('/Counselor/Event/Create-New-Event')}}" >

                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Available Seats:</label>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <input type="number" id="available_seats" name="available_seats" class="form-control"  required="required">
                                    </div>
                                    <label class="control-label col-xs-12 col-sm-1 col-md-1 col-lg-1">Type:</label>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                        <select class="select2_single form-control" name="event_type" id="event_type">
                                            <option value="Seminar">Seminar</option>
                                            <option value="Workshop">Workshop</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Name:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{csrf_field()}}
                                        <input type="text" id="event_name" name="event_name" class="form-control" placeholder="Enter event name.."  required="required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Details:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea rows="5" cols="30" id="event_content" name="event_content" style="max-width: 100%;" class="form-control" placeholder="Event brief..." required="required" ></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Venue:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea rows="5" cols="20" id="event_venue" name="event_venue" style="max-width: 100%;" class="form-control" placeholder="Venue..." required="required" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Upload Picture:</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <label class="input-group-btn">
                                                    <span class="btn btn-success">
                                                        Browse&hellip; <input type="file" name="event_img" id="event_img" class="form-control " style="display: none;" multiple>
                                                    </span>
                                            </label>
                                            <input type="text" class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Date and Time:</label>

                                    <div class="controls col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                            <input type="text" name="reservation_time" id="reservation-time" class="form-control" value="01/01/2016 - 01/25/2016" />
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <hr>
                                        <button type="submit" class="btn btn-success">Add</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="">
                                            <ul class="to_do" style="margin-top: 10px;">
                                                @foreach($errors->all() as $error)
                                                    <li>
                                                        <p><span class="flat" style="color: red;"> {{$error}}</span></p>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </form>


                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection



@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- Dropzone.js -->
    <script src="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.js")}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
    <!-- iCheck -->
    <script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset("vendors/backend_vendors/moment/min/moment.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js")}}"></script>
    <!-- jQuery-hotkeys -->
    <script src="{{ asset("vendors/backend_vendors/jquery.hotkeys/jquery.hotkeys.js")}}"></script>
    <!-- google-code-prettify -->
    <script src="{{ asset("vendors/backend_vendors/google-code-prettify/src/prettify.js")}}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset("vendors/backend_vendors/jquery.tagsinput/src/jquery.tagsinput.js")}}"></script>
    <!-- Switchery -->
    <script src="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.js")}}"></script>
    <!-- Select2 -->
    <script src="{{ asset("vendors/backend_vendors/select2/dist/js/select2.full.min.js")}}"></script>
    <!-- Parsley -->
    <script src="{{ asset("vendors/backend_vendors/parsleyjs/dist/parsley.min.js")}}"></script>
    <!-- devbridge-autocomplete -->
    <script src="{{ asset("vendors/backend_vendors/devbridge-autocomplete/dist/jquery.autocomplete.js")}}"></script>
    <!-- starrr -->
    <script src="{{ asset("vendors/backend_vendors/starrr/dist/starrr.js")}}"></script>
    <!-- pnotify -->
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>
    @include('laravelPnotify::notify')


    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>
    <script>
        $(function() {

            // We can attach the `fileselect` event to all file inputs on the page
            $(document).on('change', ':file', function() {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            // We can watch for our custom `fileselect` event like this
            $(document).ready( function() {
                $(':file').on('fileselect', function(event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }

                });
            });

        });
    </script>
@endsection

@extends('layouts.counselor_layout.counselor_design')


@section('title')
    Edit Profile
@endsection


@section('css-content')

    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset("vendors/backend_vendors/google-code-prettify/bin/prettify.min.css")}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset("vendors/backend_vendors/select2/dist/css/select2.min.css")}}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.css")}}" rel="stylesheet">
    <!-- starrr -->
    <link href="{{ asset("vendors/backend_vendors/starrr/dist/starrr.css")}}" rel="stylesheet">

    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}"  rel="stylesheet">

    <!-- PNotify -->
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.css")}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

@endsection

@section('content')
    <?php
        use App\Counselor;
        $user_further_detail = Counselor::where(["user_id"=> $user_detail->id])->first();
//        print_r($user_further_detail->Industry);

    ?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Setting</h3>
                </div>

            </div>
            <div class="clearfix"></div>


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Profile</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @if (Session::has('notifier.notice'))
                            <script>
                               new PNotify({!! Session::get('notifier.notice') !!});
                            </script>
                        @endif
                        <form action="" class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-12 text-right">Role: </label>
                                <label class="label label-primary" style="text-transform: capitalize;">{{$user_detail->user_type}}</label>
                            </div>
                        </form>


                        <div class="x_content">
                                <div class="ln_solid"></div>
                            <h4>Upload Profile Picture</h4>
                            <p class="font-gray-dark">
                                Using the grid system you can control the position of the labels. The form example below has the <b>col-md-10</b> which sets the width to 10/12 and <b>center-margin</b> which centers it.
                            </p>
                            <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" action="{{url('/Counselor/Update-Profile-Picture')}}">

                                <div class="form-group">
                                    <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                                <label class="input-group-btn">
                                                    <span class="btn btn-success">
                                                        Browse&hellip; <input type="file" name="profile_img" id="profile_img" class="form-control " style="display: none;" multiple>
                                                    </span>
                                                </label>
                                            {{csrf_field()}}
                                            <input type="text" class="form-control" readonly>
                                            <button type="submit" name="upload_btn" class="btn btn-success" style="position: absolute;margin-left: -1px;">Upload</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                                <div class="ln_solid"></div>

                            <h4>Edit Personal Info </h4>
                            <p class="font-gray-dark">
                                Using the grid system you can control the position of the labels. The form example below has the <b>col-md-10</b> which sets the width to 10/12 and <b>center-margin</b> which centers it.
                            </p>

                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  method="post" action="{{url('/Counselor/Update-Profile-Details')}}" >
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{csrf_field()}}
                                        <input type="text" id="name" name="name" class="form-control" value="{{$user_detail->name}}" required="required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="nickname" name="nickname" value="{{$user_detail->nickname}}" class="form-control" placeholder="Name" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="email" name="email" value="{{$user_detail->email}}" class="form-control" placeholder="Email" disabled="disabled">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="dob" name="dob" value="{{ \Carbon\Carbon::parse($user_detail->dob)->format('d-m-Y') }}" class="form-control" placeholder="Date Of Birth" required="required" disabled="disabled">
                                    </div>
                                </div>

                                {{--<div class="form-group">--}}
                                    {{--<label class="col-md-3 col-sm-3 col-xs-12 control-label">Gender--}}
                                    {{--</label>--}}

                                    {{--<div class="col-md-9 col-sm-9 col-xs-12">--}}

                                        {{--@if($user_detail->Gender == "male")--}}
                                            {{--<div class="radio">--}}
                                                {{--<label>--}}
                                                    {{--<input type="radio" class="flat" checked name="iCheck" disabled="disabled"> Male--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--@elseif($user_detail->Gender == "male")--}}
                                            {{--<div class="radio">--}}
                                                {{--<label>--}}
                                                     {{--<input type="radio" class="flat" name="iCheck" disabled="disabled"> Female--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--@endif--}}


                                       {{--</div>--}}
                                {{--</div>--}}


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="city" name="city" class="form-control" value="{{$user_detail->city}}" required="required" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="country" name="country" class="form-control" value="{{$user_detail->country}}" required="required" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Cell No</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" id="Cell" name="cell" class="form-control"  value="{{$user_detail->cell_no}}">
                                    </div>
                                </div>

                                <h4>Edit Career Info </h4>
                                <p class="font-gray-dark">
                                    Using the grid system you can control the position of the labels. The form example below has the <b>col-md-10</b> which sets the width to 10/12 and <b>center-margin</b> which centers it.
                                </p>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Summary</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea rows="5" cols="30" id="summary" name="summary" class="form-control" placeholder="Tell your story! Include information about your work experience, values, personal, hobbies etc." required="required" >{{$user_further_detail->summary}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Qualification</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea rows="5" cols="30" id="qualification" name="qualification" class="form-control" placeholder="qualification" required="required" >{{$user_detail->qualification}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Designation</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="designation" name="designation" class="form-control"  value="{{$user_detail->designation}}" required="required" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Working Experience</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="experience" name="experience" class="form-control" value="{{$user_further_detail->experience}}" placeholder="Experience"  >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Certification</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="certification" name="certification" class="form-control"  value="{{$user_detail->certification}}" required="required" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Industry</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="select2_single form-control" name="industry" tabindex="-1">
                                            <option>Industry</option>
                                            <option value="information-technology"@if($user_further_detail->industry == 'information-technology')selected="selected" @endif>Information Technology</option>
                                            <option value="education-training"@if($user_further_detail->industry == 'education-training')selected="selected" @endif>Education/Training</option>
                                            <option value="engineering"@if($user_further_detail->industry == 'engineering')selected="selected" @endif>Engineering</option>
                                            <option value="services"@if($user_further_detail->industry == 'services')selected="selected" @endif>Services</option>
                                            <option value="telecommunication-isp"@if($user_further_detail->industry == 'telecommunication-isp')selected="selected" @endif>Telecommunication / ISP</option>
                                            <option value="advertising-pr"@if($user_further_detail->industry == 'advertising-pr')selected="selected" @endif>Advertising / PR</option>
                                            <option value="banking-financial-services"@if($user_further_detail->industry == 'banking-financial-services')selected="selected" @endif>Banking/Financial Services</option>
                                            <option value="consultants"@if($user_further_detail->industry == 'consultants')selected="selected" @endif>Consultants</option>
                                            <option value="business-development"@if($user_further_detail->Industry == 'business-development')selected="selected" @endif>Business Development</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Functional Area</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="select2_single form-control" name="functionalarea" tabindex="-1">
                                            <option>{{$user_further_detail->functional_area}}</option>
                                            <option value="Administration"@if($user_further_detail->functional_area == 'Administration')selected="selected" @endif>Administration </option>
                                            <option value="Advertising"@if($user_further_detail->functional_area == 'Advertising')selected="selected" @endif>Advertising </option>
                                            <option value="Architects / Construction"@if($user_further_detail->functional_area == '')selected="selected" @endif>Architects &amp; Construction </option>
                                            <option value="Client Services / Customer Support"@if($user_further_detail->functional_area == 'Client Services / Customer Support')selected="selected" @endif>Client Services &amp; Customer Support </option>
                                            <option value="Computer Networking"@if($user_further_detail->functional_area == 'Computer Networking')selected="selected" @endif>Computer Networking </option>
                                            <option value="Corporate Affairs"@if($user_further_detail->functional_area == 'Corporate Affairs')selected="selected" @endif>Corporate Affairs </option>
                                            <option value="Creative Design"@if($user_further_detail->functional_area == 'Creative Design')selected="selected" @endif>Creative Design </option>
                                            <option value="Data Entry"@if($user_further_detail->functional_area == 'Data Entry')selected="selected" @endif>Data Entry </option>
                                            <option value="Database Administration (DBA)"@if($user_further_detail->functional_area == 'Database Administration (DBA)')selected="selected" @endif>Database Administration (DBA) </option>
                                            <option value="Distribution / Logistics"@if($user_further_detail->functional_area == 'Distribution / Logistics')selected="selected" @endif>Distribution &amp; Logistics </option>
                                            <option value="Engineering"@if($user_further_detail->functional_area == 'Engineering')selected="selected" @endif>Engineering </option>
                                            <option value="Executive Management"@if($user_further_detail->functional_area == 'Executive Management')selected="selected" @endif>Executive Management </option>
                                            <option value="Hardware"@if($user_further_detail->functional_area == 'Hardware')selected="selected" @endif>Hardware </option>
                                            <option value="Health / Medicine"@if($user_further_detail->functional_area == 'Health / Medicine')selected="selected" @endif>Health &amp; Medicine </option>
                                            <option value="Hotel/Restaurant Management"@if($user_further_detail->functional_area == 'Hotel/Restaurant Management')selected="selected" @endif>Hotel/Restaurant Management </option>
                                            <option value="Human Resources"@if($user_further_detail->functional_area == 'Human Resources')selected="selected" @endif>Human Resources </option>
                                            <option value="Import / Export"@if($user_further_detail->functional_area == 'Import / Export')selected="selected" @endif>Import &amp; Export </option>
                                            <option value="Industrial Production"@if($user_further_detail->functional_area == 'Industrial Production')selected="selected" @endif>Industrial Production </option>
                                            <option value="Legal Affairs"@if($user_further_detail->functional_area == 'Legal Affairs')selected="selected" @endif>Legal Affairs </option>
                                            <option value="Management Consulting"@if($user_further_detail->functional_area == 'Management Consulting')selected="selected" @endif>Management Consulting </option>
                                            <option value="Management Information System (MIS)"@if($user_further_detail->functional_area == 'Management Information System (MIS)')selected="selected" @endif>Management Information System (MIS) </option>
                                            <option value="Manufacturing"@if($user_further_detail->functional_area == 'Manufacturing')selected="selected" @endif>Manufacturing </option>
                                            <option value="Marketing"@if($user_further_detail->functional_area == 'Marketing')selected="selected" @endif>Marketing </option>
                                            <option value="Media - Print / Electronic"@if($user_further_detail->functional_area == 'Media - Print / Electronic')selected="selected" @endif>Media - Print &amp; Electronic </option>
                                            <option value="Planning / Development"@if($user_further_detail->functional_area == 'Planning / Development')selected="selected" @endif>Planning &amp; Development </option>
                                            <option value="Product Development"@if($user_further_detail->functional_area == 'Product Development')selected="selected" @endif>Product Development </option>
                                            <option value="Product Management"@if($user_further_detail->functional_area == 'Product Management')selected="selected" @endif>Product Management </option>
                                            <option value="Public Relations"@if($user_further_detail->functional_area == 'Public Relations')selected="selected" @endif>Public Relations </option>
                                            <option value="Quality Assurance (QA)"@if($user_further_detail->functional_area == 'Quality Assurance (QA)')selected="selected" @endif>Quality Assurance (QA) </option>
                                            <option value="Quality Control"@if($user_further_detail->functional_area == 'Quality Control')selected="selected" @endif>Quality Control </option>
                                            <option value="Researcher"@if($user_further_detail->functional_area == 'Researcher')selected="selected" @endif>Researcher </option>
                                            <option value="Sales / Business Development"@if($user_further_detail->functional_area == 'Sales / Business Development')selected="selected" @endif>Sales &amp; Business Development </option>
                                            <option value="Search Engine Optimization (SEO)"@if($user_further_detail->functional_area == 'Search Engine Optimization (SEO)')selected="selected" @endif>Search Engine Optimization (SEO) </option>
                                            <option value="Security"@if($user_further_detail->functional_area == 'Security')selected="selected" @endif>Security </option>
                                            <option value="Software / Web Development"@if($user_further_detail->functional_area == 'Software / Web Development')selected="selected" @endif>Software &amp; Web Development </option>
                                            <option value="Supply Chain Management"@if($user_further_detail->functional_area == 'Supply Chain Management')selected="selected" @endif>Supply Chain Management </option>
                                            <option value="Systems Analyst"@if($user_further_detail->functional_area == 'Systems Analyst')selected="selected" @endif>Systems Analyst </option>
                                            <option value="Teachers/Education, Training / Development"@if($user_further_detail->functional_area == 'Teachers/Education, Training / Development')selected="selected" @endif>Teachers/Education, Training &amp; Development </option>
                                            <option value="Writer"@if($user_further_detail->functional_area == 'Writer')selected="selected" @endif>Writer </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Skills</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="tags_1" name="skills" type="text" class="tags form-control"  value="{{$user_detail->skills}}" />
                                        <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <hr>
                                        <button type="submit" class="btn btn-success">Update Profile</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="">
                                            <ul class="to_do" style="margin-top: 10px;">
                                                @foreach($errors->all() as $error)
                                                    <li>
                                                        <p><span class="flat" style="color: red;"> {{$error}}</span></p>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </form>


                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection



@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- Dropzone.js -->
    <script src="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.js")}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
    <!-- iCheck -->
    <script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset("vendors/backend_vendors/moment/min/moment.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js")}}"></script>
    <!-- jQuery-hotkeys -->
    <script src="{{ asset("vendors/backend_vendors/jquery.hotkeys/jquery.hotkeys.js")}}"></script>
    <!-- google-code-prettify -->
    <script src="{{ asset("vendors/backend_vendors/google-code-prettify/src/prettify.js")}}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset("vendors/backend_vendors/jquery.tagsinput/src/jquery.tagsinput.js")}}"></script>
    <!-- Switchery -->
    <script src="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.js")}}"></script>
    <!-- Select2 -->
    <script src="{{ asset("vendors/backend_vendors/select2/dist/js/select2.full.min.js")}}"></script>
    <!-- Parsley -->
    <script src="{{ asset("vendors/backend_vendors/parsleyjs/dist/parsley.min.js")}}"></script>
    <!-- devbridge-autocomplete -->
    <script src="{{ asset("vendors/backend_vendors/devbridge-autocomplete/dist/jquery.autocomplete.js")}}"></script>
    <!-- starrr -->
    <script src="{{ asset("vendors/backend_vendors/starrr/dist/starrr.js")}}"></script>

    <!-- pnotify -->
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>
    @include('laravelPnotify::notify')


    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>


    <script>
        $(function() {

//            $(".ui-pnotify-container").css("display:none");

            // We can attach the `fileselect` event to all file inputs on the page
            $(document).on('change', ':file', function() {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            // We can watch for our custom `fileselect` event like this
            $(document).ready( function() {
                $(':file').on('fileselect', function(event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }

                });
            });

        });
    </script>

@endsection

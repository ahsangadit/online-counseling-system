@extends('layouts.counselor_layout.counselor_design')


@section('title')
    Profile
@endsection

@section('css-content')

    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.css")}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset("vendors/backend_vendors/select2/dist/css/select2.min.css")}}" rel="stylesheet">
    <!-- PNotify -->
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.css")}}" rel="stylesheet">

    <!-- Lightbox -->
    <link href="{{ asset("vendors/backend_vendors/light-box/css/lightbox.css")}}" rel="stylesheet">
{{--    <link href="{{ asset("vendors/backend_vendors/light-box/css/screen.css")}}" rel="stylesheet">--}}


    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

    <style>
       .list-inline-border-bottom-0{
           border-bottom: 0px !important;
       }
    </style>

@endsection



@section('content')
<?php
use Illuminate\Support\Facades\Auth;

$user_detail = Auth::user();
$user_directory = replaceByDash($user_detail->name);
$user_further_detail = \App\counselor::where(["user_id"=>$user_detail->id])->first();




 $counselor_post = \App\Counselor::find($user_further_detail->id)->posts;

// $counselor_post = \App\post::where(['counselor_id'=>$user_further_detail->id])->first();
// $post_images = explode(',',$counselor_post->post_image);



// echo $user_detail;
?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">


            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>User Profile <small>Activity report</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        @if (Session::has('notifier.notice'))
                            <script>
                                new PNotify({!! Session::get('notifier.notice') !!});
                            </script>
                        @endif
                        <div class="x_content">
                            <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                                <div class="profile_img">
                                    <div id="crop-avatar">

                                        {{--<a class="example-image-link" href="" data-lightbox="example-1"><img class="example-image" src="{{setProfilePicture()}}" alt="image-1" /></a>--}}
                                        <img class="img-responsive avatar-view" src="{{setProfilePicture()}}" alt="Avatar" title="Change the avatar">

                                        <!-- Current avatar -->
                                            {{--<a class="example-image-link" href="{{url('images/frontend_images/profile/counselors/Ahsan_1/man.png')}}" data-lightbox="example-1"><img class="img-responsive avatar-view" src="{{setProfilePicture()}}" alt="Avatar" title="Change the avatar"></a>--}}
                                    </div>
                                </div>
                                <h3>{{ $user_detail->name}}</h3>

                                <ul class="list-unstyled user_data">
                                    <li><i class="fa fa-map-marker user-profile-icon"></i> {{ $user_detail->city}},{{$space = " "}}{{$user_detail->Country}}
                                    </li>

                                    <li>
                                        <i class="fa fa-briefcase user-profile-icon"></i> {{$user_detail->designation}}
                                    </li>
                                    <li>
                                        <i class="fa fa-graduation-cap user-profile-icon"></i> {{$user_detail->qualification}}

                                    </li>

                                    <li>
                                        <i class="fa  fa-birthday-cake user-profile-icon"></i>
                                        {{\Carbon\Carbon::parse($user_detail->dob)->format('d-m-Y')}}
                                    </li>
                                    {{--<li class="m-top-xs">--}}
                                        {{--<i class="fa fa-external-link user-profile-icon"></i>--}}

                                    {{--</li>--}}
                                </ul>

                                <a class="btn btn-success" href="{{url('Counselor/Setting/Edit-Profile')}}"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                                <br />

                                <!-- start skills -->
                                <h4>Skills</h4>
                                <ul class="list-unstyled user_data">
                                    <li>
                                        <p>
                                            @foreach (explode(',',$user_detail->skills) as $skills)
                                                <label class="btn btn-success btn-xs">{{$skills}}</label>
                                            @endforeach
                                        </p>
                                    </li>
                                </ul>
                                <!-- end of skills -->

                            </div>


                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#tab_content1" role="tab" id="home-tab"  data-toggle="tab" aria-expanded="true">Profile</a>
                                        </li>

                                        <li role="presentation" class=""><a href="#tab_content2" id="timeline-tab2" role="tab" data-toggle="tab" aria-expanded="false">Timeline</a>
                                        </li>

                                        <li role="presentation" class=""><a href="#tab_content3" id="profile-tab3" role="tab" data-toggle="tab" aria-expanded="false">Recent Activity</a>
                                        </li>

                                    </ul>
                                    <div id="myTabContent" class="tab-content">

                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                            <div class="x_title">
                                                <h2>Summary</h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <p> {{$user_further_detail->summary}} </p>

                                            <!-- Large modal -->
                                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg">Update Summary</button>

                                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">Update Summary</h4>
                                                        </div>
                                                        <form method="post" action="{{url('/Counselor/Update-Profile-Details-Summary')}}">
                                                            <div class="modal-body">
                                                                <textarea rows="5" class="resizable_textarea form-control" id="summary" name="summary" style="max-width: 100%;" placeholder="This text area automatically resizes its height as you fill
                                                                in more text courtesy of autosize-master it out...">{{$user_further_detail->summary}}</textarea>


                                                            </div>
                                                            <div class="modal-footer">
                                                                {{csrf_field()}}
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-success">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div role="tabpanel" class="tab-pane fade " id="tab_content2" aria-labelledby="timeline-tab">
                                            <!-- start recent activity -->
                                            <div class="x_title">
                                                <h2>Posts</h2>

                                                <div class="clearfix"></div>
                                                <!-- Large modal -->
                                                <button type="button" class="btn btn-success btn-sm" id="basisModal-btn" data-toggle="modal" data-target="#newPostModal">New Posts</button>

                                            </div>

                                            @if(isset($counselor_post) && count($counselor_post) > 0)
                                                <div class="x_content">
                                                    <ul class="list-unstyled timeline">
                                                        @foreach($counselor_post as $key => $post)
                                                            <li>
                                                                <div class="block">
                                                                    <div class="tags">
                                                                        <a href="{{url('/Single-Post?post_id='.$post->id)}}" target="_blank" class="tag">
                                                                            <span>Post #{{$key+1}}</span>
                                                                        </a>
                                                                    </div>
                                                                    <div class="block_content">
                                                                        <h2 class="title">
                                                                            <a>{{$post->post_title}}</a>
                                                                        </h2>
                                                                        <div class="byline">
                                                                            <span>{{\Carbon\Carbon::parse($post->created_at)->format('d-m-Y')}} {{date('g:i a' , strtotime($post->created_at))}}</span>
                                                                        </div>
                                                                        <ul class="list-inline">
                                                                            <li class="inline-item list-inline-border-bottom-0"><i class="fa fa-heart" ></i> 23 Likes</li>
                                                                            <li class="inline-item list-inline-border-bottom-0"><i class="fa fa-comment"></i> 12 Comments</li>
                                                                            <li class="inline-item list-inline-border-bottom-0">
                                                                                <i class="fa fa-trash"></i><a class="delete_link" post_id="{{$post->id}}" href="#">Delete Post</a>
                                                                            </li>

                                                                        </ul>

                                                                        {{--data-toggle="modal" data-target="#basicModal"--}}
                                                                        <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-bullhorn"></i> Alert</h4>
                                                                                    </div>
                                                                                    <form method="post" id="delete-modal-form">
                                                                                        <div class="modal-body">
                                                                                            <label class="">You want to sure delete this post ?</label>
                                                                                            {{csrf_field()}}
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <a type="submit" class="btn btn-danger modal_delete_link" href="">Yes</a>
                                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @elseif(isset($counselor_post) && count($counselor_post) == 0 )
                                                <div class="text-center"><h2>No Posts</h2></div>
                                            @endif


                                            <div class="modal fade" id="newPostModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title" id="myModalLabel">New Post</h4>
                                                        </div>
                                                        <form method="post" data-parsley-validate action="{{url('/Counselor/New-Post')}}" enctype="multipart/form-data">
                                                            <div class="modal-body">

                                                                    <label class="">Title: </label>
                                                                    {{csrf_field()}}
                                                                    <input type="text" id="post_title" name="post_title" class="form-control" placeholder="Enter post title..." >
                                                                    <br>
                                                                    <label class="">Content: </label>
                                                                    <textarea rows="5" class="resizable_textarea form-control" id="post_content" name="post_content" style="max-width: 100%;" placeholder="Enter your post content..."></textarea>
                                                                    <br>
                                                                    <label class="">Upload pictures: </label>
                                                                    <div class="input-group">
                                                                        <label class="input-group-btn">
                                                                                <span class="btn btn-success">
                                                                                    Browse&hellip; <input type="file" name="post_imgs[]" id="post_imgs" class="form-control " style="display: none;" multiple>
                                                                                </span>
                                                                        </label>
                                                                        <input type="text" class="form-control" readonly>
                                                                    </div>
                                                            </div>
                                                            <div class="modal-footer">

                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="submit" name="post_btn" class="btn btn-success">Share</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <div role="tabpanel" class="tab-pane fade " id="tab_content3" aria-labelledby="profile-tab">
                                            <!-- start recent activity -->
                                            <ul class="messages">
                                                <li>
                                                    <img src="{{ profile_img_placeholder() }}" class="avatar" alt="Avatar">
                                                    <div class="message_date">
                                                        <h3 class="date text-info">24</h3>
                                                        <p class="month">May</p>
                                                    </div>
                                                    <div class="message_wrapper">
                                                        <h4 class="heading">Desmond Davison</h4>
                                                        <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                                        <br />
                                                        <p class="url">
                                                            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                            <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                                        </p>
                                                    </div>
                                                </li>
                                            </ul>
                                            <!-- end recent activity -->
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection


@section('js-content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <!-- jQuery -->
{{--    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>--}}
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- morris -->
    <script src="{{ asset("vendors/backend_vendors/raphael/raphael.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/morris/morris.js")}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
    {{--<!-- bootstrap-daterangepicker -->--}}
    {{--<script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>--}}
    {{----}}
    {{--<script src="{{ asset("vendors/backend_vendors/morris/morris.js")}}"></script>--}}
    <!-- dropzone -->
    <script src="{{ asset("vendors/backend_vendors/dropzone/dist/dropzone.js")}}"></script>
    <!-- pnotify -->
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>

    <!-- Lightbox -->

    <script src="{{ asset("vendors/backend_vendors/light-box/js/lightbox.js")}}"></script>

    @include('laravelPnotify::notify')
    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>


    <script>

        $(document).ready(function () {

            $('.delete_link').on('click' , function(){
                var id = $(this).attr("post_id");
                $('.modal_delete_link').attr("href","{{ url('/Counselor/DeletePost?post_id') }}" + "=" + id);
                $("#basicModal").modal('show');
            });

        });

        $(function() {

            // We can attach the `fileselect` event to all file inputs on the page
            $(document).on('change', ':file', function() {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            // We can watch for our custom `fileselect` event like this
            $(document).ready( function() {
                $(':file').on('fileselect', function(event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }

                });
            });

        });
    </script>
@endsection
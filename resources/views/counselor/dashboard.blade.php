@extends('layouts.counselor_layout.counselor_design')


@section('title')
    Dashboard - Counselor
@endsection

@section('css-content')

    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css")}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset("vendors/backend_vendors/jqvmap/dist/jqvmap.min.css")}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}"  rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

@endsection

@section('content')

    <?php
    use Illuminate\Support\Facades\Auth;

    $user_detail = Auth::user();
    $user_directory = replaceByDash($user_detail->name);
    $user_further_detail = \App\counselor::where(["user_id"=>$user_detail->id])->first();




    $counselor_post = \App\Counselor::find($user_further_detail->id)->posts;

    // $counselor_post = \App\post::where(['counselor_id'=>$user_further_detail->id])->first();
    // $post_images = explode(',',$counselor_post->post_image);



    // echo $user_detail;
    ?>


    <!-- page content -->
    <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-phone"></i></div>
                <div class="count">0</div>

                <h3>Talks</h3>
                <p>
                </p>
            </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-sitemap"></i></div>
                <div class="count">{{$subscribers}}</div>
                <h3>Followers</h3>
                <p></p>
            </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-comments-o"></i></div>
                <div class="count">{{$posts}}</div>
                <h3>Posts</h3>
                <p></p>
            </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="glyphicon glyphicon-blackboard"></i></div>
                <div class="count">{{$events}}</div>
                <h3>Events</h3>
                <p></p>
            </div>
        </div>
        <!-- /top tiles -->

        <div class="row">

            <div class="col-md-6 col-sm-4 col-xs-12">
                <div class="x_panel">
                        <div class="x_title">
                            <h2>Your Posts</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @if(isset($counselor_post) && count($counselor_post) > 0)
                            <div class="x_content">
                                <ul class="list-unstyled timeline">
                                    @foreach($counselor_post as $key => $post)
                                        <li>
                                            <div class="block">
                                                <div class="tags">
                                                    <a href="{{url('/Single-Post?post_id='.$post->id)}}" target="_blank" class="tag">
                                                        <span>Post #{{$key+1}}</span>
                                                    </a>
                                                </div>
                                                <div class="block_content">
                                                    <h2 class="title">
                                                        <a>{{$post->post_title}}</a>
                                                    </h2>
                                                    <div class="byline">
                                                        <span>{{\Carbon\Carbon::parse($post->created_at)->format('d-m-Y')}} {{date('g:i a' , strtotime($post->created_at))}}</span>
                                                    </div>
                                                    {{--<ul class="list-inline">--}}
                                                        {{--<li class="inline-item list-inline-border-bottom-0"><i class="fa fa-heart" ></i> 23 Likes</li>--}}
                                                        {{--<li class="inline-item list-inline-border-bottom-0"><i class="fa fa-heart" ></i> 23 Likes</li>--}}
                                                        {{--<li class="inline-item list-inline-border-bottom-0"><i class="fa fa-comment"></i> 12 Comments</li>--}}
                                                        {{--<li class="inline-item list-inline-border-bottom-0">--}}
                                                            {{--<i class="fa fa-trash"></i><a class="delete_link" post_id="{{$post->id}}" href="#">Delete Post</a>--}}
                                                        {{--</li>--}}

                                                    {{--</ul>--}}

                                                    {{--data-toggle="modal" data-target="#basicModal"--}}
                                                    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-bullhorn"></i> Alert</h4>
                                                                </div>
                                                                <form method="post" id="delete-modal-form">
                                                                    <div class="modal-body">
                                                                        <label class="">You want to sure delete this post ?</label>
                                                                        {{csrf_field()}}
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <a type="submit" class="btn btn-danger modal_delete_link" href="">Yes</a>
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @elseif(isset($counselor_post) && count($counselor_post) == 0 )
                            <div class="text-center"><h2>No Posts</h2></div>
                        @endif









                </div>
            </div>






        </div>

        <div class="row">



        </div>
    </div>
    <!-- /page content -->

@endsection


@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- Chart.js -->
    <script src="{{ asset("vendors/backend_vendors/Chart.js/dist/Chart.min.js")}}"></script>
    <!-- gauge.js -->
    <script src="{{ asset("vendors/backend_vendors/gauge.js/dist/gauge.min.js")}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
    <!-- iCheck -->
    <script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>
    <!-- Skycons -->
    <script src="{{ asset("vendors/backend_vendors/skycons/skycons.js")}}"></script>
    <!-- Flot -->
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.pie.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.time.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.stack.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.resize.js")}}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset("vendors/backend_vendors/flot.orderbars/js/jquery.flot.orderBars.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/flot-spline/js/jquery.flot.spline.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/flot.curvedlines/curvedLines.js")}}"></script>
    <!-- DateJS -->
    <script src="{{ asset("vendors/backend_vendors/DateJS/build/date.js")}}"></script>
    <!-- JQVMap -->
    <script src="{{ asset("vendors/backend_vendors/jqvmap/dist/jquery.vmap.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jqvmap/dist/maps/jquery.vmap.world.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jqvmap/examples/js/jquery.vmap.sampledata.js")}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset("vendors/backend_vendors/moment/min/moment.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>

@endsection
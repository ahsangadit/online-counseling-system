<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('images/backend_images/img/fav.png')}}">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>@yield('title')</title>

    <!--
            Google Font
            ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,500,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500i" rel="stylesheet">

    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="{{asset('css/backend_css/linearicons.css')}}">
    <link rel="stylesheet" href="{{asset('css/backend_css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/backend_css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/backend_css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('css/backend_css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('css/backend_css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/backend_css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/backend_css/main.css')}}">
</head>

<body>

@include('layouts.main_layout.header')

@yield('content')

@include('layouts.main_layout.footer')

<script src="{{asset('vendors/backend_vendors/jquery-2.2.4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="{{asset('vendors/backend_vendors/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="{{asset('js/backend_js/easing.min.js')}}"></script>
<script src="{{asset('js/backend_js/hoverIntent.js')}}"></script>
<script src="{{asset('js/backend_js/superfish.min.js')}}"></script>
<script src="{{asset('js/backend_js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('js/backend_js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/backend_js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/backend_js/owl-carousel-thumb.min.js')}}"></script>
<script src="{{asset('js/backend_js/jquery.sticky.js')}}"></script>
<script src="{{asset('js/backend_js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/backend_js/parallax.min.js')}}"></script>
<script src="{{asset('js/backend_js/waypoints.min.js')}}"></script>
<script src="{{asset('js/backend_js/wow.min.js')}}"></script>
<script src="{{asset('js/backend_js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('js/backend_js/mail-script.js')}}"></script>
<script src="{{asset('js/backend_js/main.js')}}"></script>
</body>

</html>
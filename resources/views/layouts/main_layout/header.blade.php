<!-- Start Header Area -->
<header id="header">
    <div class="container">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
               <a href="#"><img src="{{asset('images/backend_images/img/logo.png')}}" alt="" title="" /></a>
            </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li class="menu-has-children"><a href="">Pages</a>
                        <ul>
                            <li><a href="{{url('Admin/Portal')}}">Admin Portal</a></li>
                            <li><a href="{{url('Customer/Portal')}}">Customer Portal</a></li>
                            <li><a href="{{url('Counselor/Portal')}}">Counselor Portal</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="{{url('Account-Login')}}">Login</a></li>
                    <li><a href="{{url('Account-SignUp')}}">Sign up</a></li>

                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </div>
</header>
<!-- End Header Area -->
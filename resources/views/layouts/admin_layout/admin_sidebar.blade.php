                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="{{url('Admin/Portal')}}"><i class="fa fa-home"></i>Home</a></li>

                            <li><a href="{{url('Admin/Profile')}}"><i class="fa fa-user"></i>Profile</a></li>
                            <li><a><i class="fa fa-cog"></i>Setting <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('Admin/Setting/Edit-Profile')}}">Edit Profile</a></li>
                                    <li><a href="{{url('Admin/Setting/Change-Password')}}">Change Password</a></li>
                                </ul>

                            </li>

                            <li><a href="{{url('Admin/Generate-Report')}}"><i class="fa fa-file"></i> Generate Report</a></li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <h3>Extra</h3>
                        <ul class="nav side-menu">
                            <li><a href="{{url('Admin/Site-Statistic')}}"><i class="fa fa-line-chart"></i>Site Statistic</a></li>
                            <li><a href="{{url('Admin/Active-Users/')}}"><i class="fa fa-circle"></i>Active Users</a></li>
                            <li><a><i class="fa fa-users"></i>Users Details <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('Admin/Users-Details/Counselors')}}">Counselors</a></li>
                                    <li><a href="{{url('Admin/Users-Details/Customers')}}">Customers</a></li>
                                </ul>
                            </li>
                            <li><a href="{{url('Admin/Visitors')}}"><i class="fa fa-eye"></i>Visitors</a></li>
                            <li><a href="{{url('Admin/Post-Handling')}}"><i class="fa fa-cogs"></i>Post Handling</a></li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->
<!-- jQuery -->
<script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>

<!-- Bootstrap -->
<script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>

<!-- FastClick -->
<script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>

<!-- NProgress -->
<script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>

<!-- Chart.js -->
<script src="{{ asset("vendors/backend_vendors/Chart.js/dist/Chart.min.js")}}"></script>

<!-- gauge.js -->
<script src="{{ asset("vendors/backend_vendors/gauge.js/dist/gauge.min.js")}}"></script>

<!-- bootstrap-progressbar -->
<script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>

<!-- iCheck -->
<script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>

<!-- Skycons -->
<script src="{{ asset("vendors/backend_vendors/skycons/skycons.js")}}"></script>

<!-- Flot -->
<script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.pie.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.time.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.stack.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.resize.js")}}"></script>

<!-- Flot plugins -->
<script src="{{ asset("vendors/backend_vendors/flot.orderbars/js/jquery.flot.orderBars.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/flot-spline/js/jquery.flot.spline.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/flot.curvedlines/curvedLines.js")}}"></script>

<!-- DateJS -->
<script src="{{ asset("vendors/backend_vendors/DateJS/build/date.js")}}"></script>

<!-- JQVMap -->
<script src="{{ asset("vendors/backend_vendors/jqvmap/dist/jquery.vmap.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/jqvmap/dist/maps/jquery.vmap.world.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/jqvmap/examples/js/jquery.vmap.sampledata.js")}}"></script>

<!-- bootstrap-daterangepicker -->
<script src="{{ asset("vendors/backend_vendors/moment/min/moment.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>

<!-- validator -->
<script src="{{ asset("vendors/backend_vendors/validator/validator.js")}}"></script>

<!-- Datatables -->
<script src="{{ asset("vendors/backend_vendors/datatables.net/js/jquery.dataTables.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/dataTables.buttons.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.flash.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.html5.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.print.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-keytable/js/dataTables.keyTable.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-responsive/js/dataTables.responsive.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/datatables.net-scroller/js/dataTables.scroller.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/jszip/dist/jszip.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/pdfmake/build/pdfmake.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/pdfmake/build/vfs_fonts.js")}}"></script>

<!-- Autosize -->
<script src="{{ asset("vendors/backend_vendors/autosize/dist/autosize.min.js")}}"></script>

<!-- bootstrap-wysiwyg -->
<script src="{{ asset("vendors/backend_vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js")}}"></script>

<!-- Cropper.js -->
<script src="{{ asset("vendors/backend_vendors/cropper//dist/cropper.js")}}"></script>


<!-- devbridge-autocomplete -->
<script src="{{ asset("vendors/backend_vendors/devbridge-autocomplete/dist/jquery.autocomplete.js")}}"></script>

<!-- dropzone -->
<script src="{{ asset("vendors/backend_vendors/dropzone/dist/dropzone.js")}}"></script>

<!-- eCharts -->
<script src="{{ asset("vendors/backend_vendors/echarts/dist/echarts.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/echarts/map/js/world.js")}}"></script>

<!-- eve -->
<script src="{{ asset("vendors/backend_vendors/eve/eve.js")}}"></script>

<!-- FullCalendar -->
<script src="{{ asset("vendors/backend_vendors/fullcalendar/dist/fullcalendar.min.js")}}"></script>

<!-- google-code-prettify -->
<script src="{{ asset("vendors/backend_vendors/google-code-prettify/src/prettify.js")}}"></script>


<!-- ion.rangeSlider -->
<script src="{{ asset("vendors/backend_vendors/ion.rangeSlider/js/ion.rangeSlider.min.js")}}"></script>

<!-- jQuery-knob -->
<script src="{{ asset("vendors/backend_vendors/jquery-knob/js/jquery.knob.js")}}"></script>

<!-- jQuery-mousewheel -->
<script src="{{ asset("vendors/backend_vendors/jquery-mousewheel/jquery.mousewheel.js")}}"></script>

<!-- jQuery-Smart-Wizard -->
<script src="{{ asset("vendors/backend_vendors/jQuery-Smart-Wizard/jQuery.smart.wizard.js")}}"></script>

<!-- jQuery-sparkline -->
<script src="{{ asset("vendors/backend_vendors/jquery-sparkline/dist/jquery.sparkline.js")}}"></script>

<!-- jQuery-easy-pie-chart -->
<script src="{{ asset("vendors/backend_vendors/jquery-easy-pie-chart/dist/jquery.easypiechart.js")}}"></script>

<!-- jQuery-hotkeys -->
<script src="{{ asset("vendors/backend_vendors/jquery.hotkeys/jquery.hotkeys.js")}}"></script>

<!-- jQuery-inputmask -->
<script src="{{ asset("vendors/backend_vendors/jquery.inputmask/jquery.inputmask.bundle.js")}}"></script>

<!-- jQuery Tags Input -->
<script src="{{ asset("vendors/backend_vendors/jquery.tagsinput/src/jquery.tagsinput.js")}}"></script>

<!-- jszip -->
<script src="{{ asset("vendors/backend_vendors/jszip/dist/jszip.min.js")}}"></script>

<!-- mocha -->
<script src="{{ asset("vendors/backend_vendors/mocha/mocha.js")}}"></script>


<!-- morris -->
<script src="{{ asset("vendors/backend_vendors/morris/morris.js")}}"></script>

<!-- Parsley -->
<script src="{{ asset("vendors/backend_vendors/parsleyjs/dist/parsley.min.js")}}"></script>

<!-- pdfmake -->
<script src="{{ asset("vendors/backend_vendors/pdfmake/build/pdfmake.min.js")}}"></script>
<script src="{{ asset("vendors/backend_vendors/pdfmake/build/vfs_fonts.js")}}"></script>

<!-- pnotify -->
<script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>

<!-- raphael -->
<script src="{{ asset("vendors/backend_vendors/raphael/raphael.js")}}"></script>

<!-- reqirejs -->
<script src="{{ asset("vendors/backend_vendors/reqirejs/reqirejs.js")}}"></script>

<!-- Select2 -->
<script src="{{ asset("vendors/backend_vendors/select2/dist/js/select2.full.min.js")}}"></script>

<!-- starrr -->
<script src="{{ asset("vendors/backend_vendors/starrr/dist/starrr.js")}}"></script>

<!-- Switchery -->
<script src="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.js")}}"></script>

<!-- transitionize -->
<script src="{{ asset("vendors/backend_vendors/transitionize/dist/transitionize.min.js")}}"></script>


<!-- Custom Theme Scripts -->
<script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>




{{--===========================================================================================================--}}

{{--<!-- Autosize -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/autosize/dist/autosize.min.js")}}"></script>--}}

{{--<!-- bootstrap-daterangepicker -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>--}}

{{--<!-- bootstrap-datetimepicker -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js")}}"></script>--}}

{{--<!-- bootstrap-progressbar -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>--}}

{{--<!-- bootstrap-wysiwyg -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js")}}"></script>--}}

{{--<!-- Chart.js -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/Chart.js/dist/Chart.min.js")}}"></script>--}}

{{--<!-- Cropper.js -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/cropper//dist/cropper.js")}}"></script>--}}

{{--<!-- DateJS -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/DateJS/build/date.js")}}"></script>--}}

{{--<!-- Datatables -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net/js/jquery.dataTables.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/dataTables.buttons.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.flash.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.html5.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.print.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-keytable/js/dataTables.keyTable.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-responsive/js/dataTables.responsive.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/datatables.net-scroller/js/dataTables.scroller.min.js")}}"></script>--}}

{{--<!-- devbridge-autocomplete -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/devbridge-autocomplete/dist/jquery.autocomplete.js")}}"></script>--}}

{{--<!-- dropzone -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/dropzone/dist/dropzone.js")}}"></script>--}}

{{--<!-- eCharts -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/echarts/dist/echarts.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/echarts/map/js/world.js")}}"></script>--}}

{{--<!-- eve -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/eve/eve.js")}}"></script>--}}

{{--<!-- FastClick -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>--}}

{{--<!-- Flot -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.pie.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.time.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.stack.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.resize.js")}}"></script>--}}

{{--<!-- Flot-spline -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/flot-spline/js/jquery.flot.spline.min.js")}}"></script>--}}

{{--<!-- Flot-curvedline -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/flot.curvedlines/curvedLines.js")}}"></script>--}}

{{--<!-- Flot-orderbars -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/flot.orderbars/js/jquery.flot.orderBars.js")}}"></script>--}}

{{--<!-- FullCalendar -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/fullcalendar/dist/fullcalendar.min.js")}}"></script>--}}

{{--<!-- gauge.js -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/gauge.js/dist/gauge.min.js")}}"></script>--}}

{{--<!-- google-code-prettify -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/google-code-prettify/src/prettify.js")}}"></script>--}}

{{--<!-- iCheck -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>--}}

{{--<!-- ion.rangeSlider -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/ion.rangeSlider/js/ion.rangeSlider.min.js")}}"></script>--}}

{{--<!-- jQuery-knob -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/jquery-knob/js/jquery.knob.js")}}"></script>--}}

{{--<!-- jQuery-mousewheel -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/jquery-mousewheel/jquery.mousewheel.js")}}"></script>--}}

{{--<!-- jQuery-Smart-Wizard -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/jQuery-Smart-Wizard/jQuery.smart.wizard.js")}}"></script>--}}

{{--<!-- jQuery-sparkline -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/jquery-sparkline/dist/jquery.sparkline.js")}}"></script>--}}

{{--<!-- jQuery-easy-pie-chart -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/jquery-easy-pie-chart/dist/jquery.easypiechart.js")}}"></script>--}}

{{--<!-- jQuery-hotkeys -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/jquery.hotkeys/jquery.hotkeys.js")}}"></script>--}}

{{--<!-- jQuery-inputmask -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/jquery.inputmask/jquery.inputmask.bundle.js")}}"></script>--}}

{{--<!-- jQuery Tags Input -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/jquery.tagsinput/src/jquery.tagsinput.js")}}"></script>--}}

{{--<!-- JQVMap -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/jqvmap/dist/jquery.vmap.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/jqvmap/dist/maps/jquery.vmap.world.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/jqvmap/examples/js/jquery.vmap.sampledata.js")}}"></script>--}}

{{--<!-- jszip -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/jszip/dist/jszip.min.js")}}"></script>--}}

{{--<!-- mocha -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/mocha/mocha.js")}}"></script>--}}

{{--<!-- moment -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/moment/min/moment.min.js")}}"></script>--}}

{{--<!-- morris -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/morris/morris.js")}}"></script>--}}

{{--<!-- NProgress -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>--}}

{{--<!-- Parsley -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/parsleyjs/dist/parsley.min.js")}}"></script>--}}

{{--<!-- pdfmake -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/pdfmake/build/pdfmake.min.js")}}"></script>--}}
{{--<script src="{{ asset("vendors/backend_vendors/pdfmake/build/vfs_fonts.js")}}"></script>--}}

{{--<!-- pnotify -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>--}}

{{--<!-- raphael -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/raphael/raphael.js")}}"></script>--}}

{{--<!-- reqirejs -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/reqirejs/reqirejs.js")}}"></script>--}}

{{--<!-- Select2 -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/select2/dist/js/select2.full.min.js")}}"></script>--}}

{{--<!-- Skycons -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/skycons/skycons.js")}}"></script>--}}

{{--<!-- starrr -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/starrr/dist/starrr.js")}}"></script>--}}

{{--<!-- Switchery -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.js")}}"></script>--}}

{{--<!-- transitionize -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/transitionize/dist/transitionize.min.js")}}"></script>--}}

{{--<!-- validator -->--}}
{{--<script src="{{ asset("vendors/backend_vendors/validator/validator.js")}}"></script>--}}
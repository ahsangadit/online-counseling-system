@extends('layouts.admin_layout.admin_design')


@section('title')
    Edit Profile
@endsection

@section('css-content')
    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset("vendors/backend_vendors/google-code-prettify/bin/prettify.min.css")}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset("vendors/backend_vendors/select2/dist/css/select2.min.css")}}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.css")}}" rel="stylesheet">
    <!-- starrr -->
    <link href="{{ asset("vendors/backend_vendors/starrr/dist/starrr.css")}}" rel="stylesheet">

    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}"  rel="stylesheet">
    <!-- PNotify -->
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.css")}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">
@endsection


@section('content')
    <?php
    use App\administrator;
    $user_further_detail = administrator::where(["user_id"=> $user_detail->id])->first();
    //        print_r($user_further_detail->Industry);

    ?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Setting</h3>
                </div>

            </div>
            <div class="clearfix"></div>

            @if (Session::has('notifier.notice'))
                <script>
                    new PNotify({!! Session::get('notifier.notice') !!});
                </script>
            @endif

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Profile</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>


                        <form action="" class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-12 text-right">Role: </label>
                                <label class="label label-primary">{{ ucfirst($user_detail->user_type)}}</label>
                            </div>
                        </form>

                        <div class="x_content">

                            <div class="ln_solid"></div>
                            <h4>Upload Profile Picture</h4>
                            <p class="font-gray-dark">
                                Using the grid system you can control the position of the labels. The form example below has the <b>col-md-10</b> which sets the width to 10/12 and <b>center-margin</b> which centers it.
                            </p>
                            <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" action="{{url('/Admin/Update-Profile-Picture')}}">
                                <div class="form-group">
                                    <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <label class="input-group-btn">
                                                    <span class="btn btn-success">
                                                        Browse&hellip; <input type="file" name="profile_img" id="profile_img" class="form-control " style="display: none;" multiple>
                                                    </span>
                                            </label>
                                            {{csrf_field()}}
                                            <input type="text" class="form-control" readonly>
                                            <button type="submit" name="upload_btn" class="btn btn-success" style="position: absolute;margin-left: -1px;">Upload</button>
                                        </div>
                                    </div>
                                </div>

                            </form>

                            <div class="ln_solid"></div>

                            <h4>Edit Personal Info </h4>
                            <p class="font-gray-dark">
                                Using the grid system you can control the position of the labels. The form example below has the <b>col-md-10</b> which sets the width to 10/12 and <b>center-margin</b> which centers it.
                            </p>
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  method="post" action="{{url('/Admin/Update-Profile-Details')}}">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{csrf_field()}}
                                        <input type="text" id="name" name="name" class="form-control" value="{{$user_detail->name}}" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="nickname" name="nickname" value="{{$user_detail->nickname}}" class="form-control" placeholder="Name" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="email" name="email" value="{{$user_detail->email}}" class="form-control" placeholder="Email" disabled="disabled">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="dob" name="dob" class="form-control" value="{{ \Carbon\Carbon::parse($user_detail->dob)->format('d-m-Y') }}" disabled="disabled">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="city" name="city" class="form-control" value="{{$user_detail->city}}" required="required" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="country" name="country" class="form-control" value="{{$user_detail->country}}" required="required" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Cell No</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" id="cell" name="cell" class="form-control"  value="{{$user_detail->cell_no}}">
                                    </div>
                                </div>

                                <h4>Edit Career Info </h4>
                                <p class="font-gray-dark">
                                    Using the grid system you can control the position of the labels. The form example below has the <b>col-md-10</b> which sets the width to 10/12 and <b>center-margin</b> which centers it.
                                </p>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Qualification</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea rows="5" cols="30" id="qualification" name="qualification" class="form-control" placeholder="qualification" required="required" >{{$user_detail->qualification}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Designation</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="designation" name="designation" class="form-control"  value="{{$user_detail->designation}}" required="required" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Certification</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="certification" name="certification" class="form-control"  value="{{$user_detail->certification}}" required="required" >
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Skills</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="tags_1" name="skills" type="text" class="tags form-control"  value="{{$user_detail->skills}}" />
                                        <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <hr>
                                        <button type="submit" class="btn btn-success">Update Profile</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="">
                                            <ul class="to_do" style="margin-top: 10px;">
                                                @foreach($errors->all() as $error)
                                                    <li>
                                                        <p><span class="flat" style="color: red;"> {{$error}}</span></p>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </form>


                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection


@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- Dropzone.js -->
    <script src="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.js")}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
    <!-- iCheck -->
    <script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset("vendors/backend_vendors/moment/min/moment.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js")}}"></script>
    <!-- jQuery-hotkeys -->
    <script src="{{ asset("vendors/backend_vendors/jquery.hotkeys/jquery.hotkeys.js")}}"></script>
    <!-- google-code-prettify -->
    <script src="{{ asset("vendors/backend_vendors/google-code-prettify/src/prettify.js")}}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset("vendors/backend_vendors/jquery.tagsinput/src/jquery.tagsinput.js")}}"></script>
    <!-- Switchery -->
    <script src="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.js")}}"></script>
    <!-- Select2 -->
    <script src="{{ asset("vendors/backend_vendors/select2/dist/js/select2.full.min.js")}}"></script>
    <!-- Parsley -->
    <script src="{{ asset("vendors/backend_vendors/parsleyjs/dist/parsley.min.js")}}"></script>
    <!-- devbridge-autocomplete -->
    <script src="{{ asset("vendors/backend_vendors/devbridge-autocomplete/dist/jquery.autocomplete.js")}}"></script>
    <!-- starrr -->
    <script src="{{ asset("vendors/backend_vendors/starrr/dist/starrr.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>
    @include('laravelPnotify::notify')
    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>

    <script>
        $(function() {

//            $(".ui-pnotify-container").css("display:none");

            // We can attach the `fileselect` event to all file inputs on the page
            $(document).on('change', ':file', function() {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            // We can watch for our custom `fileselect` event like this
            $(document).ready( function() {
                $(':file').on('fileselect', function(event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }

                });
            });

        });
    </script>


@endsection

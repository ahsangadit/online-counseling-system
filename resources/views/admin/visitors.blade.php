@extends('layouts.admin_layout.admin_design')


@section('title')
    Visitors
@endsection

@section('css-content')
    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css")}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset("vendors/backend_vendors/jqvmap/dist/jqvmap.min.css")}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">

    <!-- Datatables -->
    <link href="{{ asset("vendors/backend_vendors/datatables.net-bs/css/dataTables.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css")}}" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">
@endsection


@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Extra</h3>
                </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Visitors <small class="label label-danger"> Page is not functional </small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Responsive example<small>Users</small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Settings 1</a>
                                                    </li>
                                                    <li><a href="#">Settings 2</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <p class="text-muted font-13 m-b-30">
                                            Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
                                        </p>

                                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>First name</th>
                                                <th>Last name</th>
                                                <th>Position</th>
                                                <th>Office</th>
                                                <th>Age</th>
                                                <th>Start date</th>
                                                <th>Salary</th>
                                                <th>Extn.</th>
                                                <th>E-mail</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Tiger</td>
                                                <td>Nixon</td>
                                                <td>System Architect</td>
                                                <td>Edinburgh</td>
                                                <td>61</td>
                                                <td>2011/04/25</td>
                                                <td>$320,800</td>
                                                <td>5421</td>
                                                <td>t.nixon@datatables.net</td>
                                            </tr>
                                            <tr>
                                                <td>Garrett</td>
                                                <td>Winters</td>
                                                <td>Accountant</td>
                                                <td>Tokyo</td>
                                                <td>63</td>
                                                <td>2011/07/25</td>
                                                <td>$170,750</td>
                                                <td>8422</td>
                                                <td>g.winters@datatables.net</td>
                                            </tr>
                                            <tr>
                                                <td>Ashton</td>
                                                <td>Cox</td>
                                                <td>Junior Technical Author</td>
                                                <td>San Francisco</td>
                                                <td>66</td>
                                                <td>2009/01/12</td>
                                                <td>$86,000</td>
                                                <td>1562</td>
                                                <td>a.cox@datatables.net</td>
                                            </tr>
                                            <tr>
                                                <td>Brielle</td>
                                                <td>Williamson</td>
                                                <td>Integration Specialist</td>
                                                <td>New York</td>
                                                <td>61</td>
                                                <td>2012/12/02</td>
                                                <td>$372,000</td>
                                                <td>4804</td>
                                                <td>b.williamson@datatables.net</td>
                                            </tr>
                                            <tr>
                                                <td>Herrod</td>
                                                <td>Chandler</td>
                                                <td>Sales Assistant</td>
                                                <td>San Francisco</td>
                                                <td>59</td>
                                                <td>2012/08/06</td>
                                                <td>$137,500</td>
                                                <td>9608</td>
                                                <td>h.chandler@datatables.net</td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection


@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>

    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>

    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>

    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>

    <!-- Chart.js -->
    <script src="{{ asset("vendors/backend_vendors/Chart.js/dist/Chart.min.js")}}"></script>

    <!-- gauge.js -->
    <script src="{{ asset("vendors/backend_vendors/gauge.js/dist/gauge.min.js")}}"></script>

    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>

    <!-- iCheck -->
    <script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>

    <!-- Skycons -->
    <script src="{{ asset("vendors/backend_vendors/skycons/skycons.js")}}"></script>

    <!-- Flot -->
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.pie.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.time.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.stack.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.resize.js")}}"></script>

    <!-- Flot plugins -->
    <script src="{{ asset("vendors/backend_vendors/flot.orderbars/js/jquery.flot.orderBars.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/flot-spline/js/jquery.flot.spline.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/flot.curvedlines/curvedLines.js")}}"></script>

    <!-- DateJS -->
    <script src="{{ asset("vendors/backend_vendors/DateJS/build/date.js")}}"></script>

    <!-- JQVMap -->
    <script src="{{ asset("vendors/backend_vendors/jqvmap/dist/jquery.vmap.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jqvmap/dist/maps/jquery.vmap.world.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jqvmap/examples/js/jquery.vmap.sampledata.js")}}"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset("vendors/backend_vendors/moment/min/moment.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>

    <!-- validator -->
    <script src="{{ asset("vendors/backend_vendors/validator/validator.js")}}"></script>

    <!-- Datatables -->
    <script src="{{ asset("vendors/backend_vendors/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/dataTables.buttons.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.flash.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.html5.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.print.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-keytable/js/dataTables.keyTable.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-responsive/js/dataTables.responsive.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-scroller/js/dataTables.scroller.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jszip/dist/jszip.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pdfmake/build/pdfmake.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pdfmake/build/vfs_fonts.js")}}"></script>


    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>
@endsection

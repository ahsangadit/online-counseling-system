@extends('layouts.admin_layout.admin_design')


@section('title')
    Site Statistic
@endsection

@section('css-content')
    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css")}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset("vendors/backend_vendors/jqvmap/dist/jqvmap.min.css")}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">

    <!-- Datatables -->
    <link href="{{ asset("vendors/backend_vendors/datatables.net-bs/css/dataTables.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css")}}" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">
@endsection


@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Extra</h3>
                </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Site Statistic <small class="label label-danger">Page is not functional</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Visitors location <small>geo-presentation</small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Settings 1</a>
                                                    </li>
                                                    <li><a href="#">Settings 2</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="dashboard-widget-content">
                                            <div class="col-md-4 hidden-small">
                                                <h2 class="line_30">125.7k Views from 60 countries</h2>

                                                <table class="countries_list">
                                                    <tbody>
                                                    <tr>
                                                        <td>United States</td>
                                                        <td class="fs15 fw700 text-right">33%</td>
                                                    </tr>
                                                    <tr>
                                                        <td>France</td>
                                                        <td class="fs15 fw700 text-right">27%</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Germany</td>
                                                        <td class="fs15 fw700 text-right">16%</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Spain</td>
                                                        <td class="fs15 fw700 text-right">11%</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Britain</td>
                                                        <td class="fs15 fw700 text-right">10%</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="world-map-gdp" class="col-md-8 col-sm-12 col-xs-12" style="height:230px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel tile fixed_height_320">
                                    <div class="x_title">
                                        <h2>App Versions</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Settings 1</a>
                                                    </li>
                                                    <li><a href="#">Settings 2</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <h4>App Usage across versions</h4>
                                        <div class="widget_summary">
                                            <div class="w_left w_25">
                                                <span>0.1.5.2</span>
                                            </div>
                                            <div class="w_center w_55">
                                                <div class="progress">
                                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 66%;">
                                                        <span class="sr-only">60% Complete</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w_right w_20">
                                                <span>123k</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="widget_summary">
                                            <div class="w_left w_25">
                                                <span>0.1.5.3</span>
                                            </div>
                                            <div class="w_center w_55">
                                                <div class="progress">
                                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
                                                        <span class="sr-only">60% Complete</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w_right w_20">
                                                <span>53k</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="widget_summary">
                                            <div class="w_left w_25">
                                                <span>0.1.5.4</span>
                                            </div>
                                            <div class="w_center w_55">
                                                <div class="progress">
                                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                                                        <span class="sr-only">60% Complete</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w_right w_20">
                                                <span>23k</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="widget_summary">
                                            <div class="w_left w_25">
                                                <span>0.1.5.5</span>
                                            </div>
                                            <div class="w_center w_55">
                                                <div class="progress">
                                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 5%;">
                                                        <span class="sr-only">60% Complete</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w_right w_20">
                                                <span>3k</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="widget_summary">
                                            <div class="w_left w_25">
                                                <span>0.1.5.6</span>
                                            </div>
                                            <div class="w_center w_55">
                                                <div class="progress">
                                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
                                                        <span class="sr-only">60% Complete</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="w_right w_20">
                                                <span>1k</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel tile fixed_height_320 overflow_hidden">
                                    <div class="x_title">
                                        <h2>Device Usage</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>

                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table class="" style="width:100%">
                                            <tr>
                                                <th style="width:37%;">
                                                    <p>Top 5</p>
                                                </th>
                                                <th>
                                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                        <p class="">Device</p>
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                        <p class="text-right">Progress</p>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                                                </td>
                                                <td>
                                                    <table class="tile_info">
                                                        <tr>
                                                            <td>
                                                                <p><i class="fa fa-square blue"></i>IOS </p>
                                                            </td>
                                                            <td>30%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p><i class="fa fa-square green"></i>Android </p>
                                                            </td>
                                                            <td>10%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p><i class="fa fa-square purple"></i>Blackberry </p>
                                                            </td>
                                                            <td>20%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p><i class="fa fa-square aero"></i>Symbian </p>
                                                            </td>
                                                            <td>15%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p><i class="fa fa-square red"></i>Others </p>
                                                            </td>
                                                            <td>30%</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel tile fixed_height_320">
                                    <div class="x_title">
                                        <h2>Quick Settings</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Settings 1</a>
                                                    </li>
                                                    <li><a href="#">Settings 2</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="dashboard-widget-content">
                                            <ul class="quick-list">
                                                <li><i class="fa fa-calendar-o"></i><a href="#">Settings</a>
                                                </li>
                                                <li><i class="fa fa-bars"></i><a href="#">Subscription</a>
                                                </li>
                                                <li><i class="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
                                                <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
                                                </li>
                                                <li><i class="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
                                                <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
                                                </li>
                                                <li><i class="fa fa-area-chart"></i><a href="#">Logout</a>
                                                </li>
                                            </ul>

                                            <div class="sidebar-widget">
                                                <h4>Profile Completion</h4>
                                                <canvas width="150" height="80" id="chart_gauge_01" class="" style="width: 160px; height: 100px;"></canvas>
                                                <div class="goal-wrapper">
                                                    <span id="gauge-text" class="gauge-value pull-left">0</span>
                                                    <span class="gauge-value pull-left">%</span>
                                                    <span id="goal-text" class="goal-value pull-right">100%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection


@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>

    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>

    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>

    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>

    <!-- Chart.js -->
    <script src="{{ asset("vendors/backend_vendors/Chart.js/dist/Chart.min.js")}}"></script>

    <!-- gauge.js -->
    <script src="{{ asset("vendors/backend_vendors/gauge.js/dist/gauge.min.js")}}"></script>

    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>

    <!-- iCheck -->
    <script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>

    <!-- Skycons -->
    <script src="{{ asset("vendors/backend_vendors/skycons/skycons.js")}}"></script>

    <!-- Flot -->
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.pie.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.time.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.stack.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/Flot/jquery.flot.resize.js")}}"></script>

    <!-- Flot plugins -->
    <script src="{{ asset("vendors/backend_vendors/flot.orderbars/js/jquery.flot.orderBars.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/flot-spline/js/jquery.flot.spline.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/flot.curvedlines/curvedLines.js")}}"></script>

    <!-- DateJS -->
    <script src="{{ asset("vendors/backend_vendors/DateJS/build/date.js")}}"></script>

    <!-- JQVMap -->
    <script src="{{ asset("vendors/backend_vendors/jqvmap/dist/jquery.vmap.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jqvmap/dist/maps/jquery.vmap.world.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jqvmap/examples/js/jquery.vmap.sampledata.js")}}"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset("vendors/backend_vendors/moment/min/moment.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>

    <!-- validator -->
    <script src="{{ asset("vendors/backend_vendors/validator/validator.js")}}"></script>

    <!-- Datatables -->
    <script src="{{ asset("vendors/backend_vendors/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/dataTables.buttons.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.flash.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.html5.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.print.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-keytable/js/dataTables.keyTable.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-responsive/js/dataTables.responsive.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-scroller/js/dataTables.scroller.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jszip/dist/jszip.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pdfmake/build/pdfmake.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pdfmake/build/vfs_fonts.js")}}"></script>


    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>
@endsection

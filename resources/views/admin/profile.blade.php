@extends('layouts.admin_layout.admin_design')


@section('title')
    Profile
@endsection

@section('css-content')
    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">
@endsection


@section('content')
    <?php
    $user_detail = Auth::user();
    $user_further_detail = \App\Administrator::where(['User_id'=>$user_detail->id])->first();
    ?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>User Profile</h3>
                </div>


            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>User Report <small>Activity report</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                                <div class="profile_img">
                                    <div id="crop-avatar">
                                        <!-- Current avatar -->
                                        <img class="img-responsive avatar-view" src="{{setProfilePicture()}}" alt="Avatar" title="Change the avatar">
                                    </div>
                                </div>
                                <small style="text-transform:capitalize;font-weight: 700;margin-top: -5px;"></small>
                                <h3 style="text-transform: capitalize;">{{Auth::user()->name}}</h3>

                                <ul class="list-unstyled user_data">
                                    <li>
                                        <i class="glyphicon glyphicon-user"></i> <b style="text-transform: capitalize"> {{Auth::user()->user_type}}</b>
                                    </li>
                                    <li><i class="fa fa-map-marker user-profile-icon"></i>  {{ $user_detail->city}},{{$space = " "}}{{$user_detail->country}}
                                    </li>
                                    <li>
                                        <i class="fa fa-briefcase user-profile-icon"></i> {{$user_detail->Designation}}
                                    </li>
                                    <li>
                                        <i class="fa fa-graduation-cap user-profile-icon"></i> {{$user_detail->qualification}}
                                    </li>
                                    <li>
                                        <i class="fa  fa-birthday-cake user-profile-icon"></i>
                                        {{\Carbon\Carbon::parse($user_detail->dob)->format('d-m-Y')}}
                                    </li>
                                    <li>
                                       <b> Rights</b> :{{$user_further_detail->access_right}}
                                    </li>
                                </ul>

                                <a class="btn btn-success" href="{{url('Admin/Setting/Edit-Profile')}}"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                                <br />

                                <!-- start skills -->
                                <h4>Skills</h4>
                                <ul class="list-unstyled user_data">
                                    <li>
                                        <p>
                                            @foreach (explode(',',$user_detail->skills) as $skills)
                                                <label class="btn btn-success btn-xs">{{$skills}}</label>
                                            @endforeach
                                        </p>
                                    </li>
                                </ul>
                                <!-- end of skills -->

                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <div class="profile_title">
                                    <div class="col-md-6">
                                        <h2>User Activity Report</h2>
                                    </div>

                                </div>

                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        {{--<li role="presentation" class=""><a href="#tab_content1" role="tab" id="home-tab"  data-toggle="tab" aria-expanded="false">Profile</a>--}}
                                        {{--</li>--}}
                                        <li role="presentation" class="active"><a href="#tab_content2" id="profile-tab2" role="tab" data-toggle="tab" aria-expanded="true">Recent Activity</a>
                                        </li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        {{--<div role="tabpanel" class="tab-pane fade" id="tab_content1" aria-labelledby="home-tab">--}}
                                            {{--<h2>Summay</h2>--}}
                                            {{--<p>{{$user_further_detail->}}</p>--}}
                                        {{--</div>--}}

                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content2" aria-labelledby="profile-tab">

                                            <!-- start recent activity -->
                                            <ul class="messages">
                                                <li>
                                                    <img src="{{ profile_img_placeholder() }}" class="avatar" alt="Avatar">
                                                    <div class="message_date">
                                                        <h3 class="date text-info">24</h3>
                                                        <p class="month">May</p>
                                                    </div>
                                                    <div class="message_wrapper">
                                                        <h4 class="heading">Desmond Davison</h4>
                                                        <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                                        <br />
                                                        <p class="url">
                                                            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                            <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                                        </p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <img src="{{ profile_img_placeholder() }}" class="avatar" alt="Avatar">
                                                    <div class="message_date">
                                                        <h3 class="date text-error">21</h3>
                                                        <p class="month">May</p>
                                                    </div>
                                                    <div class="message_wrapper">
                                                        <h4 class="heading">Brian Michaels</h4>
                                                        <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                                        <br />
                                                        <p class="url">
                                                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                                                            <a href="#" data-original-title="">Download</a>
                                                        </p>
                                                    </div>
                                                </li>
                                            </ul>
                                            <!-- end recent activity -->

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection

@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- morris -->
    <script src="{{ asset("vendors/backend_vendors/raphael/raphael.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/morris/morris.js")}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/morris/morris.js")}}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>
@endsection

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class counselor extends Model
{
    //

    protected $fillable = ['user_id', 'experience', 'summary', 'industry', 'functional_area', 'facebook_link', 'linkedin_link', 'twitter_link'];

    public function counselor()
    {
        return $this->belongsTo('App\counselor');
    }

    public function services()
    {
        return $this->belongsToMany('App\service');
    }

    public function events()
    {
        return $this->belongsToMany('App\event');
    }

    public function subscribers(){
        return $this->belongsToMany('App\subscriber');
    }

    public function posts(){
        return $this->belongsToMany('App\post');
    }
}

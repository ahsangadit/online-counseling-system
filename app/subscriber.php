<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subscriber extends Model
{
    //
    protected $fillable = ['customer_id','counselor_id','status','attribute'];
}

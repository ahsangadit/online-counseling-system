<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    //

    protected $fillable = ['counselor_id','post_title','post_content','post_image'];
}

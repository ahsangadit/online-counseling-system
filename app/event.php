<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    //
    protected $fillable = ['counselor_id','event_name','event_type','event_content','event_image','event_seats','event_venue','event_datetime'];
}

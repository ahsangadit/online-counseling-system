<?php

namespace App\Http\Controllers;

use App\Customers_Comment;
use Illuminate\Http\Request;

class CustomersCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customers_Comment  $customers_Comment
     * @return \Illuminate\Http\Response
     */
    public function show(Customers_Comment $customers_Comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customers_Comment  $customers_Comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Customers_Comment $customers_Comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customers_Comment  $customers_Comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customers_Comment $customers_Comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customers_Comment  $customers_Comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customers_Comment $customers_Comment)
    {
        //
    }
}

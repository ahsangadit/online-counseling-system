<?php

namespace App\Http\Controllers;

use App\Counselor_Location;
use Illuminate\Http\Request;

class CounselorLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Counselor_Location  $counselor_Location
     * @return \Illuminate\Http\Response
     */
    public function show(Counselor_Location $counselor_Location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Counselor_Location  $counselor_Location
     * @return \Illuminate\Http\Response
     */
    public function edit(Counselor_Location $counselor_Location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Counselor_Location  $counselor_Location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Counselor_Location $counselor_Location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Counselor_Location  $counselor_Location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Counselor_Location $counselor_Location)
    {
        //
    }
}

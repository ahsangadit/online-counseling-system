<?php

namespace App\Http\Controllers;

use App\Counselor;
use App\event;
use App\post;
use App\subscriber;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jleon\LaravelPnotify\Notify;

class CounselorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function counselorLayoutDesign(){
        return view('layouts.counselor_layout.counselor_design',array('user_detail'=>Auth::user()));
    }

    public function portal(){


        $counselor = counselor::where(['user_id'=>Auth::user()->id])->first();
        $subscribers = subscriber::where(['counselor_id'=>$counselor->id])->where(['status'=>'Followed'])->count();

        $posts = post::where(['counselor_id'=>$counselor->id])->count();
        $events = event::where(['counselor_id'=>$counselor->id])->count();

        return view('counselor.dashboard',array('subscribers'=>$subscribers,'posts'=>$posts,'events'=>$events));
    }

    public function profile(){
        return view('counselor.profile');
    }

    public function updateProfilePicture(Request $request){

        $user = Auth::user();

        if($file = $request->file('profile_img')){
            $filename = $file->getClientOriginalName();
            $user_directory = replaceByDash($user->name);
            $file->move('images/frontend_images/profile/counselors/'.$user_directory.'_'.$user->id.'/',$filename);
            $input = $filename;
            User::where('id',Auth::user()->id)->update(['profile_img'=>$input]);
        }
        Notify::success('Picture Updated Successfully', 'Profile Picture');
        return redirect('/Counselor/Profile');
    }

    public function editProfile(){
        return view('counselor.setting-editProfile',array('user_detail'=>Auth::user()));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfileDetails(Request $request){

        $user_id = Auth::user()->id;

        $this->validate($request,[
            'name'=> 'required|min:5|max:15','nickname'=>'min:5',
            'city' => 'required','country'=>'required','skills'=>'required',
            "experience" => 'required',
            'cell'=> 'required','designation'=>'required','qualification'=>'required','certification'=>'required',
            'industry'=>'required','functionalarea'=>'required'
        ]);


        User::where(['id'=>$user_id])->update(["name"=>$request->name , "nickname"=>$request->nickname , 'city'=>$request->city ,
            "country"=>$request->country , "skills"=>$request->skills ,"cell_no" => $request->cell ,
            "designation" => $request->designation , "qualification" => $request->qualification,
            "certification" => $request->certification
        ]);

        Counselor::where(["user_id"=>$user_id])->update(["industry" => $request->industry , "experience"=>$request->experience,
            "functional_area" => $request->functionalarea,"summary"=>$request->summary]);


        Notify::success('Record Updated Successfully', 'Profile Updated');
        return redirect()->action('CounselorController@editProfile');

    }

    public function updateProfileSummary(Request $request){
        $user_id = Auth::user()->id;

        if($request->summary){
            Counselor::where(['user_id'=>$user_id])->update(['summary'=>$request->summary]);

            Notify::success('Summary Updated Successfully', 'Profile Summary Updated');
            return redirect()->action('CounselorController@profile');
        }else{
            Notify::danger('Something went wrong.Profile Summary not update !!', 'Profile Summary');
            return redirect()->action('CounselorController@profile');
        }
    }

    public function changePassword(){
        return view('counselor.setting-changePassword');
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Counselor  $counselor
     * @return \Illuminate\Http\Response
     */
    public function show(Counselor $counselor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Counselor  $counselor
     * @return \Illuminate\Http\Response
     */
    public function edit(Counselor $counselor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Counselor  $counselor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Counselor $counselor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Counselor  $counselor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Counselor $counselor)
    {
        //
    }
}

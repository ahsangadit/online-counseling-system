<?php

namespace App\Http\Controllers;

use App\Counselor_Feedback;
use Illuminate\Http\Request;

class CounselorFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Counselor_Feedback  $counselor_Feedback
     * @return \Illuminate\Http\Response
     */
    public function show(Counselor_Feedback $counselor_Feedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Counselor_Feedback  $counselor_Feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Counselor_Feedback $counselor_Feedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Counselor_Feedback  $counselor_Feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Counselor_Feedback $counselor_Feedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Counselor_Feedback  $counselor_Feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Counselor_Feedback $counselor_Feedback)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Providing_TimeSlot;
use Illuminate\Http\Request;

class ProvidingTimeSlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Providing_TimeSlot  $providing_TimeSlot
     * @return \Illuminate\Http\Response
     */
    public function show(Providing_TimeSlot $providing_TimeSlot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Providing_TimeSlot  $providing_TimeSlot
     * @return \Illuminate\Http\Response
     */
    public function edit(Providing_TimeSlot $providing_TimeSlot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Providing_TimeSlot  $providing_TimeSlot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Providing_TimeSlot $providing_TimeSlot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Providing_TimeSlot  $providing_TimeSlot
     * @return \Illuminate\Http\Response
     */
    public function destroy(Providing_TimeSlot $providing_TimeSlot)
    {
        //
    }
}

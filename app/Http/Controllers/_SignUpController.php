<?php

namespace App\Http\Controllers;

use App\administrator;
use App\counselor;
use App\customer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rules\In;

class _SignUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('SignUp');
    }

    public function adminSignup(){
        return view('AdminSignUp');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $role = $request->role;

        if($role == 'customer'){

            $this->validate($request,[
                'username'=> 'required|min:5|max:15','password'=>'required|min:3','nickname'=>'min:5',
                'email'=>'required','city' => 'required','country'=>'required','skills'=>'required',
                'cell'=> 'required','designation'=>'required','qualification'=>'required',
            ]);

            $customer = new User;
            $customer->name = $request->username;
            $customer->email= $request->email;
            $customer->email_verified_at= now();
            $customer->password = bcrypt($request->password);
            $customer->nickname = $request->nickname;
            $customer->dob = $request->date;
            $customer->gender = $request->gender;
            $customer->city = $request->city;
            $customer->country = $request->country;
            $customer->qualification = $request->qualification;
            $customer->designation = $request->designation;
            $customer->certification = $request->certification;
            $customer->skills = $request->skills;
            $customer->cell_no = $request->cell;
            $customer->user_type = $request->role;
            $customer->save();


            customer::create(["user_id"=>$customer->id,"talk_counts"=>0]);


        }elseif ($role == 'counselor'){

            $this->validate($request,[
                'username'=> 'required|min:5|max:15','password'=>'required|min:3','nickname'=>'min:5',
                'email'=>'required','city' => 'required','country'=>'required','skills'=>'required',
                'cell'=> 'required','designation'=>'required','qualification'=>'required','certification'=>'required',
                'industry'=>'required','functionalarea'=>'required'
            ]);

            $counselor = new User;
            $counselor->name = $request->username;
            $counselor->email= $request->email;
            $counselor->email_verified_at= now();
            $counselor->password = bcrypt($request->password);
            $counselor->nickname = $request->nickname;
            $counselor->dob = $request->date;
            $counselor->gender = $request->gender;
            $counselor->city = $request->city;
            $counselor->country = $request->country;
            $counselor->qualification = $request->qualification;
            $counselor->designation = $request->designation;
            $counselor->certification = $request->certification;
            $counselor->skills = $request->skills;
            $counselor->cell_no = $request->cell;
            $counselor->user_type = $request->role;
            $counselor->save();


            counselor::create(["user_id"=>$counselor->id,"experience"=>$request->experience,"industry"=>$request->industry,"functional_area"=>$request->functionalarea]);

        }elseif ($role == 'administrator'){

            $this->validate($request,[
                'username'=> 'required|min:5|max:15','password'=>'required|min:3','nickname'=>'min:5',
                'email'=>'required','city' => 'required','country'=>'required','skills'=>'required',
                'cell'=> 'required','designation'=>'required','qualification'=>'required',
            ]);

            $admin = new User;
            $admin->name = $request->username;
            $admin->email= $request->email;
            $admin->email_verified_at= now();
            $admin->password = bcrypt($request->password);
            $admin->nickname = $request->nickname;
            $admin->dob = $request->date;
            $admin->gender = $request->gender;
            $admin->city = $request->city;
            $admin->country = $request->country;
            $admin->qualification = $request->qualification;
            $admin->designation = $request->designation;
            $admin->certification = $request->certification;
            $admin->cell_no = $request->cell;
            $admin->skills = $request->skills;
            $admin->user_type = $request->role;
            $admin->save();

            administrator::create(["user_id"=>$admin->id,"access_right"=>"Full-Access"]);
        }else{
            echo "Something Wrong";
        }

//        $data[0] = Input::get('username');
//        $data[1] = Input::get('password');
//        $data[2] = Input::get('nickname');
//        $data[3] = Input::get('email');
//        $data[4] = Input::get('date');
//        $data[5] = Input::get('gender');
//        $data[6] = Input::get('city');
//        $data[7] = Input::get('country');
//        $data[8] = Input::get('cell');
//        $data[9] = Input::get('role');
//        $data[10] = Input::get('experience');
//        $data[11] = Input::get('designation');
//        $data[12] = Input::get('qualification');
//        $data[13] = Input::get('certification');
//        $data[14] = Input::get('skills');
//        $data[15] = Input::get('functionalarea');
//        $data[16] = Input::get('industry');

//        foreach ($data as $key => $value){
//            echo $value;
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

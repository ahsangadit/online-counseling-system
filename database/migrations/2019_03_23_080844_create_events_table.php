<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('counselor_id');
            $table->string('event_name');
            $table->string('event_content');
            $table->string('event_image');
            $table->string('event_type');
            $table->string('event_seats');
            $table->string('event_venue');
            $table->string('event_datetime');
            $table->dateTime('event_startdatetime');
            $table->dateTime('event_enddatetime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;

Route::get('/', function () { return view('welcome'); });


Route::get('/main', function () { return view('welcome-counseling-page'); });


Route::match(['get','post'],'/Account-Login' , "_LoginController@login");
Route::get('/Logout' , '_LogoutController@logout');


Route::get('/Account-SignUp', '_SignUpController@index');// customer counselor signup view
Route::get('/Admin/Account-SignUp', '_SignUpController@adminSignup');//only admin signup view

Route::get('/Account-SignUp/save', '_SignUpController@save');



Route::group(['middleware'=> ['auth','customer']],function (){
    /* =============== Customer Routes =============== */
    Route::get('/Customer/Portal', "CustomerController@portal");

    //Profile Routes Section
    Route::get('/Customer/Profile', "CustomerController@profile" );
    Route::post('/Customer/Update-Profile-Picture',"CustomerController@updateProfile");
    Route::post('/Customer/Update-Profile-Details',"CustomerController@updateProfileDetails");
    Route::get('/Customer/CounselorProfile',"CustomerController@singleCounselorProfile");

    //Setting Routes Section
    Route::get('/Customer/Setting/Edit-Profile', "CustomerController@editProfile");
    Route::get('/Customer/Setting/Change-Password', "CustomerController@changePassword");

    //Counselling Routes Section
    Route::get('/Customer/Counselling/Search-Counselors', "CustomerController@searchCounselors");
    Route::post('/Customer/Counselling/Searching-Counselors', "CustomerController@searchingCounselors");

    //Subscribe Routes Section
    Route::get('/Customer/Counselling/Subscribed-Counselors' , "CustomerController@subscribedCounselors");
    Route::get('/Customer/Subscribe',"CustomerController@subscribe");
    Route::get('/Customer/Unsubscribe',"CustomerController@unsubscribe");

    //Event Routes Section
    Route::get('Customer/Join-Events', "CustomerController@joinEvents");

    Route::get('Customer/Following', "CustomerController@following");

//    Route::get('/Single-Post',"PostShareController@singlePost");
});

Route::group(['middleware'=>['auth','counselor']],function (){
    /* =============== Counselor Routes =============== */
    Route::get('/Counselor/Portal', "CounselorController@portal");
    Route::get('/Counselor/Profile', "CounselorController@profile");
    Route::get('/Counselor/Setting/Edit-Profile' , "CounselorController@editProfile");
    Route::post('/Counselor/Update-Profile-Picture',"CounselorController@updateProfilePicture");
    Route::post('/Counselor/Update-Profile-Details',"CounselorController@updateProfileDetails");
    Route::post('/Counselor/Update-Profile-Details-Summary',"CounselorController@updateProfileSummary");
    Route::get('/Counselor/Setting/Change-Password', "CounselorController@changePassword");

    //Post Routes Section
    Route::post('/Counselor/New-Post',"PostShareController@newPost");
//    Route::get('/Counselor/SinglePost',"PostShareController@singlePost");
//    Route::get('/Single-Post',"PostShareController@singlePost");

    Route::get('/Counselor/DeletePost',"PostShareController@deletePost");
    Route::post('/Counselor/EditPost',"PostShareController@editPost");

    //Service Route Section
    Route::get('/Counselor/Service/Create-Services',"ServiceController@addServiceView");// This route return add service view
    Route::get('/Counselor/Service/View-Services',"ServiceController@viewServiceView");// This route return view service view
    Route::post('/Counselor/Service/Create-New-Service',"ServiceController@addNewService");// This route add new service in database
    Route::post('/Counselor/EditService',"ServiceController@editService");
    Route::get('/Counselor/DeleteService',"ServiceController@deleteService");

    //Event Route Section
    Route::get('/Counselor/Event/Create-Event',"EventController@addEventView");// This route return add service view
    Route::get('/Counselor/Event/View-Event',"EventController@viewEventView");// This route return view service view
    Route::post('/Counselor/Event/Create-New-Event',"EventController@addNewEvent");// This route add new service in database
    Route::post('/Counselor/EditEvent',"EventController@editEvent");
    Route::get('/Counselor/DeleteEvent', "EventController@deleteEvent");



});

Route::group(['middleware' =>['auth','admin']],function (){
    /* =============== Admin Routes =============== */
    Route::get('/Admin/Portal',  "AdministratorController@portal" );
    Route::get('/Admin/Profile',  "AdministratorController@profile");

    Route::get('/Admin/Setting/Edit-Profile' ,  "AdministratorController@editProfile");
    Route::post('/Admin/Update-Profile-Picture' ,  "AdministratorController@updateProfilePicture");
    Route::post('/Admin/Update-Profile-Details',"AdministratorController@updateProfileDetails");

    Route::get('/Admin/Setting/Change-Password', "AdministratorController@changePassword");
    Route::get('/Admin/Generate-Report',  "AdministratorController@generateReport");
    Route::get('/Admin/Site-Statistic',  "AdministratorController@siteStatistic");
    Route::get('/Admin/Active-Users',  "AdministratorController@activeUsers");
    Route::get('/Admin/Users-Details/Counselors',  "AdministratorController@counselorsDetails");
    Route::get('/Admin/Users-Details/Customers',  "AdministratorController@customersDetails");
    Route::get('/Admin/Visitors',  "AdministratorController@visitors");
    Route::get('/Admin/Post-Handling',  "AdministratorController@postHandling");
});


Route::group(['middleware'=>['auth']],function (){
        Route::get('/Single-Post',"PostShareController@singlePost");
        Route::get("/Help","GenericController@help");
});



/* ================= Below routes is for checking and update password ================= */
Route::get('/Setting/Check-Password', "GenericController@checkPassword");
Route::match(['get', 'post'],'/Setting/Update-Password', "GenericController@updatePassword");

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
